" Plugins
execute 'source' stdpath('config') . '/plugs.vim'

" Misc. Config
let g:neovide_fullscreen=v:true
let g:neovide_input_use_logo=v:true
set tabstop=4
set shiftwidth=4
set number
set noshowmode

" Aesthetics
execute 'source' stdpath('config') . '/theme.vim'

" Treesitter
lua <<EOF
require('nvim-treesitter.configs').setup {
	highlight = {
		enable = true,
	},
	rainbow = {
		enable = true,
	},
}
EOF

" Telescope setup
lua require('telescope').setup()
if !has('win32')
	lua require('telescope').load_extension('fzf')
endif

" LSP
execute 'source' stdpath('config') . '/lsp.vim'

execute 'source' stdpath('config') . '/keybinds.vim'
