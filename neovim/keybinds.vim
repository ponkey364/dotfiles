if has('macunix')
	let mapleader = '`'
endif
if has('win32')
	let mapleader = '\'
endif

" remaps
tnoremap <Esc> <C-\><C-n>

" cmd+c , cmd+v for macOS
if has('macunix')
	nmap <D-c> "+y
	vmap <D-c> "+y
	nmap <D-v> "+p
endif

nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>gs <cmd>Telescope git_status<cr>
nnoremap <leader>ls <cmd>Telescope lsp_dynamic_workspace_symbols<cr>
nnoremap <leader>li <cmd>Telescope lsp_implmentations<cr>
nnoremap <leader>lr <cmd>Telescope lsp_references<cr>
nnoremap <leader>ld <cmd>Telescope lsp_definitions<cr>
