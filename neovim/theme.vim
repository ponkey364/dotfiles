colorscheme catppuccin-mocha
set guifont=FiraCode\ Nerd\ Font:h14

lua << EOF
vim.g.lightline = {
	colorscheme= 'catppuccin',
	active= { left = {{ 'mode', 'paste' }, { 'gitbranch', 'readonly', 'filename', 'modified'}}},
	component_function= { gitbranch = 'fugitive#head' }
}
EOF
