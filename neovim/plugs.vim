call plug#begin(stdpath('data') . '/plugged')
Plug 'nvim-lua/plenary.nvim'

" Aesthetics
Plug 'catppuccin/nvim'
Plug 'folke/tokyonight.nvim'
Plug 'itchyny/lightline.vim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'p00f/nvim-ts-rainbow'

" Telescope
Plug 'nvim-telescope/telescope.nvim'
" fzf is borked on windows, I disabled it :/
if !has('win32')
	Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
endif

" Utils
Plug 'tpope/vim-fugitive'
Plug 'lewis6991/gitsigns.nvim'
Plug 'tpope/vim-commentary'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Languagues & LSP
Plug 'neovim/nvim-lspconfig'
Plug 'simrat39/rust-tools.nvim'
Plug 'sebdah/vim-delve'

call plug#end()

lua require('gitsigns').setup()
lua require('nvim-web-devicons').setup()
