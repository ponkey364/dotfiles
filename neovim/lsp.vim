lua require('rust-tools').setup({})
set completeopt=menu,menuone,noselect

lua <<EOF
	local nvim_lsp = require('lspconfig')

	local on_attach = function(client, bufnr)
		local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

		buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
	end

	local servers = {'gopls', 'dartls', 'rust_analyzer'}
	for _, lsp in pairs(servers) do 
		nvim_lsp[lsp].setup {
			on_attach = on_attach,
		}
	end
EOF
